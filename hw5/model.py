#Edwin Ortega Gomez
#AppModel class derived form IModel, function implementations

import copy
from IModel import IModel

class AppModel(IModel):

  #Reviews list [id, name, term, year, time, instructor, review, rating]
  list = [["CS410", "Spring", "2018", "14:00-16:00", "Wuchang Feng", "Great class, goodwork!", "5"],
	  ["CS494", "Winter", "2018", "18:40-20:00", "Raoul Rivas", "Great class, lots of work!", "4"],
 	  ["CS486", "Winter", "2018", "14:00-16:00", "Hisham Benotman", "Great class, learned SQL!", "4"],
          ["CS410", "Fall", "2017", "10:00-11:50", "Wuchang Feng", "Great class, learned about OWASPs top 10 vulnerabilities!", "4"]]
  """
  Constructor with argument
  """
  def __init__(self, app):
    self.arg = app

  """
  Fetch all of course review entries in the database/dictionary.
  """
  def getAll(self):
    return self.list

  """
  Add a review
  """
  def addReview(self, values):
    pass

  """
  Delete a review
  """
  def deleteReview(self, id):
    pass

  """
  Fetch the term
  """
  def getTerm(self, database_name, term):
    pass

  """
  Fetch the year
  """
  def getYear(self, database_name, year):
    pass

  """
  Fetch time course offered
  """
  def getTime(self, database_name, time):
    pass

  """
  Fetch instructor
  """
  def getInstructor(self, database_name, instructor):
    pass

  """
  Fetch review content
  """
  def getReview(self, database_name, review):
    pass

  """
  Fetch rating of review
  """
  def getRating(self, database_name, rating):
    pass
