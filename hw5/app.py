#Edwin Ortega Gomez
#Hw2: Course Review Toy App

from flask import Flask, render_template
import flask.views

from model import AppModel

app = Flask(__name__)
model = AppModel(app)


@app.route('/')
def index():
  return render_template('home.html')

@app.route('/reviews')
def pages():
  rr = model.getAll()
  return render_template('reviews.html', reviews=rr)

if __name__ == '__main__':
  app.run(host='0.0.0.0', port=8000, debug=True)
