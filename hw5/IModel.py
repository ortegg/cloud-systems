#Edwin Ortega Gomez
#Abstract base class for the course review toy app

#from abc import ABC, abstractmethod
from abc import ABCMeta, abstractmethod

class IModel:
  __metaclass__ = ABCMeta
  """
  Fetch all of course review entries in the database/dictionary.
  """
  @abstractmethod
  def getAll(self):
    pass

  """
  Add a review
  """
  @abstractmethod
  def addReview(self, values):
    pass

  """
  Delete a review
  """
  @abstractmethod
  def deleteReview(self, id):
    pass

  """
  Fetch the term
  """
  @abstractmethod
  def getTerm(self, database_name, term):
    pass

  """
  Fetch the year
  """
  @abstractmethod
  def getYear(self, database_name, year):
    pass

  """
  Fetch time course offered
  """
  @abstractmethod
  def getTime(self, database_name, time):
    pass

  """
  Fetch instructor
  """
  @abstractmethod
  def getInstructor(self, database_name, instructor):
    pass

  """
  Fetch review content
  """
  @abstractmethod
  def getReview(self, database_name, review):
    pass

  """
  Fetch rating of review
  """
  @abstractmethod
  def getRating(self, database_name, rating):
    pass
