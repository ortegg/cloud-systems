#Edwin Ortega Gomez
#AppModel class derived form IModel, function implementations

import copy
from IModel import IModel

class AppModel(IModel):
  """
  Constructor with argument
  """
  def __init__(self, app):
    self.arg = app

  """
  Fetch all of course review entries in the database/dictionary.
  """
  def getAll(self):
    pass

  """
  Add a review
  """
  def addReview(self, values):
    pass

  """
  Delete a review
  """
  def deleteReview(self, id):
    pass

  """
  Fetch the term
  """
  def getTerm(self, database_name, term):
    pass

  """
  Fetch the year
  """
  def getYear(self, database_name, year):
    pass

  """
  Fetch time course offered
  """
  def getTime(self, database_name, time):
    pass

  """
  Fetch instructor
  """
  def getInstructor(self, database_name, instructor):
    pass

  """
  Fetch review content
  """
  def getReview(self, database_name, review):
    pass

  """
  Fetch rating of review
  """
  def getRating(self, database_name, rating):
    pass
