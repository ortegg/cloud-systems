from wtforms import Form, StringField, TextAreaField, validators

#Review Form
class ReviewForm(Form):
  name = TextAreaField('Course Name', [validators.Length(min=1, max=50)])
  term =TextAreaField('Term', [validators.Length(min=1, max=10)])
  year = TextAreaField('Year', [validators.Length(min=1, max=10)])
  time =TextAreaField('Time', [validators.Length(min=1, max=20)])
  instructor =TextAreaField('Instructor', [validators.Length(min=1, max=50)])
  review = TextAreaField('Review', [validators.Length(min=1, max=350)])
  rating =TextAreaField('Rating', [validators.Length(min=1, max=1)])
