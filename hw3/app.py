#Edwin Ortega Gomez
#Hw3: Add the support of a database to the flask app

"""
Reviews Database:
+---------+---------+---------+---------+----------+---------+---------+
|Name     |Term     |Year     |Time     |Instructor|Review   |Rating   |
+=========+=========+=========+=========+==========+=========+=========+
|CS410    |Spring   |2018     |8:00-9:00|Wuchang   |Good!:)  |5 (1-5)  |
+---------+---------+---------+---------+----------+---------+---------+
"""

from flask import Flask, render_template, redirect, url_for, request
import flask.views
import requests
import sqlite3

from model import AppModel
from form import ReviewForm

app = Flask(__name__)	# flask app
model = AppModel(app)	# model object is now a flak app
DB_FILE = 'reviews.db'	# file for database of reviews

def select():
    """
    Gets all entries from the database
    """
    connection = sqlite3.connect(DB_FILE)
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM reviews")
    return cursor.fetchall()

def insert(name, term, year, time, instructor, review, rating):
    """
    Inserts entry into database
    """
    params = {'name':name, 'term':term, 'year':year, 'time':time, 'instructor':instructor, 'review':review, 'rating':rating}
    connection = sqlite3.connect(DB_FILE)
    cursor = connection.cursor()
    cursor.execute("insert into reviews (name, term, year, time, instructor, review, rating) VALUES (:name, :term, :year, :time, :instructor, :review, :rating)", params)

    connection.commit()
    cursor.close()

#This route is the default landing page for the app
@app.route('/')
def index():
  return render_template('home.html')

#This route is the page that displays the anonymous reviews
@app.route('/reviews')
def pages():
  rr = [dict(name=row[0], term=row[1], year=row[2], time=row[3], instructor=row[4], review=row[5], rating=row[6]) for row in select()]
  return render_template('reviews.html', reviews=rr)

#This route is the page to submit new reviews
@app.route('/addReview', methods=['POST', 'GET'])
def addReview():
  forms = ReviewForm(request.form)
  add = request.method == 'POST' and forms.validate()
  print(request.method)
  print(forms.validate())
  return render_template('addReview.html', form=forms, can_add=add)

#This route is to put the form data to database
@app.route('/upload', methods=['POST'])
def upload():
    """
    Accepts POST requests, and processes the form;
    Redirect to index when completed.
    """
    insert(request.form['name'], request.form['term'], request.form['year'], request.form['time'], request.form['instructor'], request.form['review'], request.form['rating']) 
    return redirect(url_for('index'))

#Listen on all IP addresses on port 8000
if __name__ == '__main__':
  
  #Make sure the database exists
  connection = sqlite3.connect(DB_FILE)
  cursor = connection.cursor()
  try:
    cursor.execute("select count(rowid) from reviews")
  except sqlite3.OperationalError:
    cursor.execute("create table reviews (name text, term text, year text, time text, instructor text, review text, rating text)")
  cursor.close()

  app.run(host='0.0.0.0', port=8000, debug=True)
